package ru.t1.zkovalenko.tm.exception.system;

public final class CommandNotSupportedException extends AbstractClassException {

    public CommandNotSupportedException() {
        super("Command not supported.");
    }

    public CommandNotSupportedException(String command) {
        super("Command '" + command + "' not supported.");
    }

}
