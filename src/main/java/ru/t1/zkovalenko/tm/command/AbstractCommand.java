package ru.t1.zkovalenko.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.model.ICommand;
import ru.t1.zkovalenko.tm.api.service.IServiceLocator;
import ru.t1.zkovalenko.tm.enumerated.Role;

@Getter
@Setter
public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getName();

    public abstract String getArgument();

    public abstract String getDescription();

    public abstract Role[] getRoles();

    @NotNull
    @Override
    public String toString() {
        final Integer spacesArgs = 7;
        final Integer spacesNames = 40;
        final String argument = getArgument();
        final String name = getName();
        final String description = getDescription();
        String result = "";
        if (argument != null && !argument.isEmpty()) result += argument;
        result += spaceCounter(argument, spacesArgs);
        if (!name.isEmpty()) result += name;
        result += spaceCounter(name, spacesNames);
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

    @NotNull
    private String spaceCounter(@Nullable String string, final Integer maxSpace) {
        if (string == null) string = "";
        int resultCountSpaces = maxSpace - string.length();
        resultCountSpaces = resultCountSpaces > 0 ? resultCountSpaces : 1;
        @NotNull String resultSpaces = "";
        for (int i = 0; i < resultCountSpaces; i++) {
            resultSpaces += " ";
        }
        return resultSpaces;
    }

    @NotNull
    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

}
