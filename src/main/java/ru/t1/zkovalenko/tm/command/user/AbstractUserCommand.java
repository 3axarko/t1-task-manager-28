package ru.t1.zkovalenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.service.IUserService;
import ru.t1.zkovalenko.tm.command.AbstractCommand;
import ru.t1.zkovalenko.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole());
    }

}
