package ru.t1.zkovalenko.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        getProjectService().create(userId, name, description);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
